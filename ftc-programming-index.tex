\documentclass{article}

\title{FTC Programming}
\author{Benjamin Lohse}

\usepackage{hyperref}

\begin{document}

\maketitle

\tableofcontents

This is an index of resources on learning topics relating to FTC programming.
The collection of resources here is meant to take one from no programming knowledge to being a highly capable FTC programmer.
Note that this guide is targeted at Android Studio/IntelliJ IDEA users because that is the only FTC development environment that I am familiar with, and many of the things I recommend are only possible with Android Studio/IntelliJ IDEA.

\section{Programming Language}

Language-specific guides can be found below.
Once you've read about a programming language, it's essential to practice using it in order to cement the concepts in your mind.
One website to do this on is Project Euler (\url{https://projecteuler.net/}).
It's a large collection of mathematical problems that are designed to be solved with programming.

\subsection{Java}

This is the biggest hurdle to learning FTC programming in my opinion.
It is also completely unavoidable.
You have to learn Java to use any of the other resources in this guide.
Here are some resources you can use to do it: \begin{itemize}
	\item \url{https://stemrobotics.cs.pdx.edu/node/4196.html} - This is what I used to learn FTC programming back in the day. I learned a lot from it at the time, but I will say that it doesn't seem to be actively developed, so some of the information may be out of date. It does have some example programms that can be helpful. The sections relating to the EV3 platform and the RoboRIO platform are for FLL and FRC programming respectively. As such, you don't need to read them for FTC.
	\item \url{https://www.amazon.com/Murachs-Java-Programming-Joel-Murach/dp/1943872074} - This is a textbook that I used to learn Java. I'm not saying that it's the best textbook for learning Java (I haven't tried any others). I am saying that I learned a lot from it. You may or may not be able to find the book for free on Library Genesis (Please note that I do not endorse or condone obtaining the book this way. I simply want to make you aware of the options). It should be noted that only the first three sections of the book are relevant to FTC programming.
	\item \url{https://www.tutorialspoint.com/java/index.htm} - This is an online course for Java. My disclaimer here is that I haven't actually used this Java course. The reason I include it here is that I have used their C programming course and found it to be quite helpful.

\end{itemize}

\subsection{Kotlin}

Kotlin is another programming language that can be used for FTC.
It runs on the JVM (Java Virtual Machine) which makes it fairly easy to include in your project.
It's similar to Java in a lot of ways, but is newer and has more modern syntax.
There aren't any huge advantages to using it unless you just prefer the syntax.
If you are interested, it's quite easy to learn once you know Java because most of the fundamental programming concepts are the same (just the syntax is different).
Once you know Java, you can learn Kotlin as you go just by looking up how to to x thing in Kotlin.
If you want a more cohesive experience you could try the following: \begin{itemize}
	\item \url{https://kotlinlang.org/docs/home.html} - This is the official Kotlin documentation and is often the first result when you look up how to do something in Kotlin.
	\item \url{https://www.tutorialspoint.com/kotlin/index.htm} - This is an online course for Kotlin. I haven't used it myself. I've simply used a C programming course by the same group.
\end{itemize}

\section{Development Environment}

An IDE (Integrated Development Environment) is an application that comes with all the tools needed to write and develop software.
IntelliJ IDEA is an open source Java IDE created by Jetbrains.
Google later based their Android IDE, Android Studio, on IntelliJ.
Android Studio is extremely similar to IntelliJ, the major difference being that Android Studio comes with the tools for Android development whereas they have to be installed separately on IntelliJ.
Android Studio is the officially supported FTC development environment, and it takes less time to set up, so it's best to use that unless you have a good reason not to.
The main reason not to is that you can use IntelliJ to write desktop apps and Android apps, but you can only use Android Studio to write Android apps.
As such, if you want to do both and have limited disk space, you may want to go with IntelliJ.

\subsection{IntelliJ IDEA}

Note that while this section is titled IntelliJ IDEA, pretty much everything in here also applies to Android Studio.

\subsection{Vim}

Vim is a text editor.
It's the text editor.
It's an extremely lightweight CLI-based text editor that's meant to be operated entirely with the keyboard.
It's a standard text editor on Linux systems and has a massive plugin ecosystem.
To use it, install Vim or Neovim (my choice).
To learn the basics of Vim, open up the editor and type :Tutor
You'll also probably want a vim plugin manager (I use packer.nvim).
Reasons to use vim: \begin{itemize}
	\item It allows for vastly more efficient text editing.
	\item If you have a computer that struggles to run Android Studio, it may be beneficial to write most of your code in vim and only use Android Studio for uploading code to the robot controller.
\end{itemize}
With the right plugins and vim configuration, vim can be made to be just as powerful as a dedicated IDE, but getting it to this state takes a lot of time and effort, so I wouldn't recommend it to most teams.
You can instead install the IDEAVim plugin on Android Studio or IntelliJ to get a lot of the benefits of vim with the convenience of a full IDE.

\section{Gradle}

\section{FTC Control System}

Once you know your way around the programming language and development environment you have to learn the robotics-specific stuff.
Places to do this: \begin{itemize}
	\item \url{https://docs.revrobotics.com/duo-control/control-hub-gs} - It's a good idea to take a look at the Rev documentation when setting up your hubs for the first time.
	\item \url{https://gm0.org/en/latest/docs/software/index.html} - Game manual 0 is a great resource for all things FTC (not just programming), but they've also got some good information on the software side of things. One of the best pieces of information I found here were bulk reads (which can help to speed up your program's cycle time).
	\item \url{https://github.com/FIRST-Tech-Challenge/FtcRobotController/wiki} - This is the official documentation for the FTC SDK. One of the most notable pieces of information here is how to write an I2C driver which is necessary to use some I2C devices in FTC.
	\item \url{https://ftctechnh.github.io/ftc_app/doc/javadoc/overview-summary.html} - These are the official FTC SDK JavaDocs. It lists all the classes found in the SDK, their superclasses, methods, etc. It's not the kind of thing that you want to read straight through, but it is a great reference tool.
	\item Game Manual 1 - The game manuals are something anyone in FTC needs to be very familiar with, but as a software developer, you need to be particularly aware of the rules listed in the robot control system and software sections of game manual 1. You need to know these sections like the back of your hand. If you don't know those rules, then nothing else in this guide will mater to you because you won't be able to make it past inspection.
\end{itemize}

\section{Control Theory}

Control theory describes the set of algorithms used to make robot movements as fast, efficient, and accurate as possible.
As such it is critical in programming robots.
Here are the resources to learn it: \begin{itemize}
	\item \url{https://acme-robotics.gitbook.io/road-runner/tour/introduction} - This is documentation for the motion control library, Road Runner (\url{https://learnroadrunner.com/}). It's a great place to learn the basics of control theory, even if you don't use Road Runner. Note that you can simply use the Road Runner library instead of writing your own control algorithms. I prefer to write my own because I learn more, enjoy the challenge, and have a better understanding of the algorithms that I'm using, but there's nothing wrong with just using Road Runner, especially if you're just getting started.
	\item \url{https://file.tavsys.net/control/controls-engineering-in-frc.pdf} - This is graduate level control theory for high school robotics. It is a wealth of knowlege, and will teach you everything you could possible want to know about control theory and more. I do have to warn you that it is extremely math intensive. I wouldn't even attempt reading this book without knowing calculus and linear algebra. If you need to learn these, I recommend the series by 3Blue1Brown on calculus (\url{https://www.youtube.com/watch?v=WUvTyaaNkzM&list=PL0-GT3co4r2wlh6UHTUeQsrf3mlS2lk6x}) and linear algebra (\url{https://www.youtube.com/watch?v=kjBOesZCoqc&list=PL0-GT3co4r2y2YErbmuJw2L5tW4Ew2O5B}).
	\item \url{https://docs.google.com/document/d/1YGRUUjW2Mp5vNpFg4KtFkLdege1_769kvKtuIxmkLYw/edit} - This is the programming journal from FTC team 11115 Gluten Free (they won back-to-back world championships in 2018 and 2019) for the Rover Rukus season. It's fairly short (it seems like he stopped updating it at some point), but it's still worth taking a look at.
	\item \url{https://efyang.dev/media/documents/efyang_2018_2019_FTC_3216_Autonomous_and_Control_Writeup.pdf} - This is another programming journal that's worth taking a look at.
	\item \url{https://github.com/nwatx/Gluten-Free-Code-Reuploaded/tree/master/ftc_app-master} - This is Gluten Free's competition code from the Rover Ruckus season. It can definately be useful to take look at if you're wondering how they did something.
	\item \url{https://www.youtube.com/watch?v=3l7ZNJ21wMo} - This is a tutorial series by Gluten Free's programmer on how to implement the pure pursuit path tracking algorithm.
\end{itemize}


\subsection{Odometry}

\section{Computer Vision}

Every recent FTC game has involved a task that is meant to be solved with computer vision.
It may be technically possible to solve some of them with alternative solutions (such as color sensors), but vision is usually the best approach.
Computer vision can also be used to determine the location of the robot (although I wouldn't recommend it), determine the position of scoring elements on the field, and target a goal during shooting games.
As such, it is a critical skill for an FTC programmer.

\subsection{OpenCV}

OpenCV is my preferred computer vision system (both for FTC and FRC) because it is fast, reliable, and open source.
OpenCV is an industry-grade computer vision library.
As such, it can be a little overwhelming at first.
Look for OpenCV courses, and you'll find extremely long courses that cost over \$1,000.
The good news is that you don't need that kind of stuff just for FTC programming.
I found this guide from FRC Programming Done Right (\url{https://frc-pdr.readthedocs.io/en/latest/vision/introduction.html}) to be a good starting point.
Although this guide is written for FRC, nearly everything in it can apply to FTC.
The math in it can be a little intimidating, but don't panic if you don't get the math.
You don't need to understand the math to use OpenCV.
You can also look at the OpenCV documentation (\url{https://frc-pdr.readthedocs.io/en/latest/vision/introduction.html} for further reading.
Once you've OpenCV, you're going to want a way to include it in your FTC project.
The easiest way to do that is the EasyOpenCV library (\url{https://github.com/OpenFTC/EasyOpenCV}) which is just a simple way to use OpenCV algorithms on FTC robots.

\section{Version Control}

A version control system manages different versions of a project across time, and devices.
It allows multiple people to collaborate on the same project, using different devices.
It also protects you from losing work by storing all previous versions of your project.
If you've ever used Google Docs, you may have noticed that is let's you see previous versions of your project and who made each edit.
This is an example of a rudimentry version control system.
A true version control system is like that on steroids.

\subsection{Git}

Git is the most popular version conrol system by far.
It's a very standard tool that can be installed on any operating system.
Git is a very complicated system, but only the basics are needed for FTC.
To learn it, I would recommend the Git Book (\url{https://git-scm.com/book/en/v2}).
Only the first three sections of the book are relevant to FTC programming (although all of it is useful if you're interested in pursueing a carrier in software development).
In order to share a git repository between multiple computers and collaborators, you need a code hosting platform.
GitHub (\url{https://github.com/}) is the most popular of these, but I've found it to be blocked at some schools.
If this is the case at your school, it may be more convenient to use one of these alternatives: \begin{itemize}
	\item GitLab (\url{https://about.gitlab.com/}) - This is the one I use. It's worked quite well. I have no complaints.
	\item BitBucket (\url{https://bitbucket.org/})
\end{itemize}
I've also found that some schools block port 22 (the port used for SSH).
This can be a problem if you're trying to clone code via SSH (you'd have to use HTTPS instead).
You can also create your own git server and host your code yourself.
This allows for absolute control over the server, but it is a very technical process, so I wouldn't recommend it to most teams.
I'd also like to emphasize the importance of good commit messages.
Well written commit messages allow you to figure out exactly what state your project was in at each point in time; therefore, you can easily determine which commit you need to fall back to if something goes wrong.

\section{Development Practices}

\subsection{Test-Driven Development}

Unit tests are the most important workflow optimization that I've learned during all my time in FTC.
The essential problem that they serve to address in FTC is that of testing time.
A normal software developer spends more time debugging their code than they did writing it.
Trying to debug code on the robot makes this process even more time consuming.
With each test the robot must be placed back in its starting position, the cod emust be reuploaded to the robot controller, and the op mode must be selected again.
This makes it very difficult to get in a large number of tests in a reasonable amount of time.
The solution is to test the bulk of your code without the robot.
This also allows for assembly to be making physical changes to the robot while development work is underway.
Furthermore, it lets programmers test their code at home without the robot.
The basic idea is to write a series of test functions that ensure your motion control algorithms are functioning properly.
You pick an algorithm, decide on sets of conditions (the algorithm's parameters), and calculate by hand what the algorithm should return under each set of conditions.
You then use assert statements (advanced testing methods can be used with the junit library (\url{https://junit.org/junit5/})) to verify that your code works properly.
I would also recommend having a function that will run all the tests in your project at once.

\subsection{Simulator}

Units tests are capable of testing control algorithms, but can be cumbersome to use for testing entire op modes.
For this, I would recommend using a robot simulator.
These are the ones I know of: \begin{itemize}
	\item \url{https://bitbucket.org/PeterTheEarthling/tutorialseries/src/master/} - Instructions on how to set it up can be found in his pure pursuit tutorial which is linked in the control theory section. This is a simple simulator that allows you to test drivetrain movements.
	\item \url{https://synthesis.autodesk.com/} - This is a three dimensional robot simulator that allows you to test all aspects of your robot. The first problem is that it takes a pretty powerful computer to run this thing well. The other is that it's designed for FRC, so you'd have to figure out how to integrate your FTC code with it.
\end{itemize}

\subsection{FTC Dashboard}

FTC dashboard (\url{https://acmerobotics.github.io/ftc-dashboard/}) is essentially an upgraded version of the driver station app that can be used for testing.
It has greater functionality than the driver station app and allows one to run the robot without a driver hub/phone, just a laptop.

\subsection{Optimization}

Below are some optimization tips to make your code run faster (faster code can make some odometry systems more accurate): \begin{itemize}
	\item Don't call on the size of an array list in the loop when iterating over it.
		Create a variable with the size of the array list outside of the loop.
	\item Switch statements are more efficient than if statements with multiple cases.
	\item Use the smallest data type that can work for each variable.
	\item Use primative types instead of reference types when possible.
	\item Use bulk reads for sensors.
	\item I2C sensors are slower than other types of sensors.
\end{itemize}

\subsection{Clean Code}

Most of this is common sense.
If you want a whole book on it, you can look at "Clean Code" by Robert C. Martin (\url{https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882}), but it's mostly pretty simple.
Make your code readable, and don't use stupid names (it makes it harder for others to understand your code).

\subsection{Documentation}

I'd say it's not really necessary for most FTC projects, but if you have a large project that's going to be around for years, it might help your future team members if you create javadocs for your code.

\section{Command-Line Interface}

It's not completely necessary, but I would recommend familiarizing yourself with the command-line interface (CLI) of whatever operating system you are using.
This is useful for debugging, issueing commands to your version control system, and writing scripts to make the development process more efficient.
It will also give you a better understanding of how your computer works.

\subsection{Linux CLI}

I'd recommend the Linux Journey website (\url{https://linuxjourney.com/}).
Only the first four sections are relevant to FTC (although the entire thing is a great resource if you are interested in IT and Linux systems).

\subsection{MacOS CLI}

\subsection{Windows CLI}

\end{document}
